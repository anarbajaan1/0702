// Массивы

// let names = Array();

// let names = [1, 5, 10, 700, 100, true, [11, 12], 'text'];

// let names = ['ivan', 'Igor', 'Ruslan', 'Timur'];

// names[2] = 'test'
// names[100] = 'sotka'

// names.push('Vika')
// names.push('Table')
// names.pop()

//метод - команда у переменной
//.push() - добавляет элемент в конце
//.pop() удаляет последний элемент
// names.shift() - удаляет с начала
// names.unshift('First') - добавляет с начала
//.concat() - соединяет массивы, не изменяет исходный массив
//.includes('Igor') - ищет значение внутри массива, если нашел то true 
//.indexOf('Vika') - показывает индекс массива
//.reverse() - перестраивает , меняет местами
// .splice(id(старт), (кол-во элементов)) - удаляет из массива n элементов со указанного индекса, ИЗМЕНЯЕТ ИСХОДНЫЙ МАССИВ
//.slice(start, end) - НЕ ИЗМЕНЯЕТ ИСХОДНЫЙ МАССИВ. Показывает элементы в диапазоне (start-end)

// let students = ['Jenys', 'Nazgul']

// let newNames = names.concat(students) 
// console.log(names[2])

// console.log(names.includes('Igor'))
// console.log(names.indexOf('Viktoria'))
// names.reverse()

// names = names.slice(2)
// names.splice(2, 2)

// console.log(names.join('-'))
// console.log('Длина, (кол-во элементов)', names.length)

// console.log(newNames)

// for(let i = 0; i < names.length; i++){
//     console.log(names[i])
// }

// for(let item in names){
//     console.log(names[item])
// }


// for(let item of names){
//     console.log(item)
// }

// const nums = [10, 0, -20, 50, 44, 7];

// let max = nums[0];
// let maxId = 0;

// for (let i in nums) {
//     if(nums[i] > max){
//         max = nums[i]
//         maxId = i;
//     }
// }

// console.log({max, maxId}) 

// for (let item of nums) {
//     if (item > max) {
//         max = item
//         maxId = nums.indexOf(item);
//     }
// }

// console.log({max, maxId})

// let nums = [1,2,3,4,6,5,10,5,60,2]
// let sum = 0

// for(let item of nums){
//     if(item % 2 === 0){
//         sum += item
//     }
// }

// console.log(sum)

// let arr = [1,2,3,4,5]
// let arr2 = Array.from(arr)
// let arr2 = [...arr]

// arr2.push(123)

// console.log(arr, arr2)
// console.log(arr === arr2)

// let arr = [1, 502, 18, 543, 89, 2, 70]

// let max = arr[0]
// let max2 = arr[0]

// for(let item of arr){
//     if (item > max){
//         max = item
//     }
// }
// console.log({max})

// for(let item of arr){
//     if(item > max2 && item < max){
//         max2 = item
//     }
// }
// console.log({max2})